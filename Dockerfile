FROM openshift/base-centos7

MAINTAINER https://gitlab.cern.ch/hse-see-co-docker/openshift-centos

LABEL io.openshift.s2i.destination="/opt/s2i/destination"

RUN INSTALL_PKGS="tar unzip bc which lsof" && \
    yum install -y --enablerepo=centosplus $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y

# Add archive and uncompress it
ADD aria2-1.31.0.tar.gz /usr/local/
#RUN tar -zxf /usr/local/aria2-1.31.0.tar.gz /usr/local && rm -f /usr/local/aria2-1.31.0.tar.gz

RUN pushd /usr/local/aria2-1.31.0 && ./configure && make && make install && popd

CMD /bin/bash